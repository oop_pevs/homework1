import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

class Logger
{
    /**
     *  Variables
     */
    private int level;
    private String filePath = "myApp.log";

    /**
     *  Constants
     */
    private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public static int LOG_LEVEL_ERROR = 0;
    public static int LOG_LEVEL_INFO = 1;
    public static int LOG_LEVEL_DEBUG = 2;

    /**
     * Function for setting log level
     * @param level
     */
    public void setLevel(int level)
    {
        this.level = level;
    }

    /**
     * Function for getting log level
     * @return
     */
    private int getLevel()
    {
        return this.level;
    }

    /**
     * Function for get actual date and time
     * @return
     */
    private String getDate()
    {
        Date date = new Date();
        return sdf.format(date);
    }

    /**
     * Function which write log data to log file
     * @param message
     * @param logLevel
     */
    private void writeLog(String message, String logLevel)
    {
        try {
            File file = new File(this.filePath);
            FileWriter fw = new FileWriter(file, true);

            fw.write(this.getDate()+" ");
            fw.write(logLevel+" ");
            fw.write(message+"_"+logLevel+"\n");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * write only error messages
     * @param message
     */
    public void error(String message)
    {
        if (this.getLevel() >= LOG_LEVEL_ERROR)
            this.writeLog(message,"error");

    }

    /**
     * write error and info messages
     * @param message
     */
    public void info(String message)
    {
        if (this.getLevel() >= LOG_LEVEL_INFO)
            this.writeLog(message,"info");
    }

    /**
     * write error, info and debug messages
     * @param message
     */
    public void debug(String message)
    {
        if (this.getLevel() >= LOG_LEVEL_DEBUG)
            this.writeLog(message,"debug");
    }

    /**
     * Main function for running / test this program
     * @param args
     */
    public static void main(String[] args)
    {
        Logger logger = new Logger();

        logger.setLevel(LOG_LEVEL_DEBUG);
        logger.info("test");
        logger.error("test");
        logger.debug("test");
    }
}
